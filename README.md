# Y

这个程序主要是治疗一些人在没有看文档的前提下提交Issue的陋习。

它将输出一些提醒阅读文档的信息，当然你也可以让它输出任何内容。

### 安装方法
1. 下载链接: https://gitee.com/heStudio/y-application/releases
2. 使用`dpkg安装`
```shell
dpkg --install y-application_*.deb
```

### 使用方法
```shell
y [file]
```

`file`不是必需的，当你没有指定时，它会显示默认内容。它仅支持文本格式

