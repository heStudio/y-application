#!/bin/bash

# y application 
#
# 醉、倾城 by heStudio
# https://www.hestudio.org/
# mailto:hestudio@hestudio.org
#

demo () {
    echo " "
    echo "看文档了吗？让你闭眼按y就真的闭眼按了吗？你有没有自己的脑子？"
    echo " "
    echo "Did you read the document? Let you close your eyes and press y really close your eyes and press it? Do you have a brain of your own?"
    echo " "
    echo "ドキュメントは読まれましたか？目をつぶってyを押せと言われたら押しましたか？自分の脳みそはあるのか？"
    echo ""
    echo "Вы читали документ? Закрывали ли вы глаза и нажимали ли клавишу 'y', когда вам говорили это сделать? У вас есть собственный мозг?"
    echo " "
    echo "Haben Sie das Dokument gelesen? Haben Sie Ihre Augen geschlossen und y gedrückt, als Sie dazu aufgefordert wurden? Haben Sie ein eigenes Gehirn?"
    echo " "
    sleep 5
    yes
}

if [[ -n $1 ]]; then
    cat $1
else
    demo
fi
